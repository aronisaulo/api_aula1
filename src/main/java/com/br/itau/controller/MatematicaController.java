package com.br.itau.controller;

import com.br.itau.models.Matematica;
import com.br.itau.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;
    @PostMapping("soma")
    public Integer soma(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() < 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario ao menos dois números para serem somados");
        }
        return  matematicaService.soma(matematica.getNumeros());
    }

    @PostMapping("subtrair")
    public Integer subtrair(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() < 2 ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario ao menos dois números para serem subtrair");
        }
        return  matematicaService.subtrair(matematica.getNumeros());
    }

    @PostMapping("multiplicar")
    public Integer multiplicar(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || numeros.size() < 2 ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario ao menos dois números para serem multiplicar");
        }
        return  matematicaService.multiplicar(matematica.getNumeros());
    }

    @PostMapping("dividir")
    public Integer dividir(@RequestBody Matematica matematica){
        List<Integer> numeros = matematica.getNumeros();
        if (numeros.isEmpty() || (numeros.get(0)<numeros.get(1)) ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario ao menos dois números e primeiro maior que o segundo para serem divididos");
        }
        return  matematicaService.dividir(matematica.getNumeros());
    }
}
