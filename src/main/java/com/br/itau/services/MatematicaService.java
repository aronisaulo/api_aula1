package com.br.itau.services;

import org.jcp.xml.dsig.internal.dom.DOMUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatematicaService {
  public  int soma(List<Integer> numeros) {
      int resultado = 0;
      for (Integer numero: numeros){
          resultado += numero;
      }
      return  resultado;
  }

    public int subtrair(List<Integer> numeros){
        int resultado =0 ;
        for (Integer numero: numeros) {
            if (resultado==0)
                  resultado = numero;
            else
                  resultado -=  numero ;
        }
        return  resultado;
    }
    public int dividir(List<Integer> numeros){
        return  numeros.get(0) / numeros.get(1) ;
    }
    public int multiplicar(List<Integer> numeros){
        int resultado = 1;
        for (Integer numero: numeros) {
             resultado *=  numero ;
        }
        return  resultado;
    }
}
