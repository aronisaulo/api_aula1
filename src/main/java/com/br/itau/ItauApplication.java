package com.br.itau;

import com.br.itau.controller.OlaMundoController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItauApplication {

	public static void main(String[] args) {

		SpringApplication.run(ItauApplication.class, args);

	}

}
